#include <rt/integrators/castingdist.h>
#include <rt/intersection.h>
#include <rt/world.h>

namespace rt {

RayCastingDistIntegrator::RayCastingDistIntegrator(World* world, const RGBColor& nearColor, float nearDist, const RGBColor& farColor, float farDist)
: Integrator(world), nearColor(nearColor), farColor(farColor), nearDist(nearDist), farDist(farDist)
{
    /* TODO */
}

RGBColor RayCastingDistIntegrator::getRadiance(const Ray& ray) const {
    Intersection intersection = world->scene->intersect(ray);
    if (intersection) {
        float bw = -rt::dot(intersection.normal(), ray.d);
        float distFactor = (intersection.distance - nearDist) / (farDist - nearDist);
        distFactor = max(min(distFactor, 1.0f), 0.0f);
        return (bw * (nearColor * (1 - distFactor) + farColor * distFactor));
    }
    else
        return RGBColor(0., 0., 0.);
}

}
