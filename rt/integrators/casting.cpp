#include <rt/integrators/casting.h>
#include <rt/intersection.h>
#include <rt/world.h>

namespace rt {

RGBColor RayCastingIntegrator::getRadiance(const Ray& ray) const {
    Intersection intersection = world->scene->intersect(ray);
    if (intersection)
    {
        float bw = -rt::dot(intersection.normal(), ray.d);
        return RGBColor(bw, bw, bw);
    }
    else
        return RGBColor(0., 0., 0.);
    }

}
