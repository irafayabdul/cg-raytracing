#include <rt/intersection.h>

namespace rt {

Intersection::Intersection(float distance, const Ray& ray, const Solid* solid, const Vector& normal, const Point& local)
: ray(ray), solid(solid), distance(distance), normall(normal), locall(local)
{

}

Intersection::operator bool() const {
    /* TODO */
    return distance != FLT_MAX;
}

Intersection Intersection::failure() {
    /* TODO */
    Intersection temp = Intersection();
    temp.distance = FLT_MAX;
    return temp;
}

Point Intersection::hitPoint() const {
    /* TODO */
    return ray.getPoint(distance);
}

Vector Intersection::normal() const {
    /* TODO */
    return normall;
}

Point Intersection::local() const {
    /* TODO */
    return locall;
}

}
