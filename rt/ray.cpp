#include <core/assert.h>
#include <core/macros.h>
#include <rt/ray.h>

namespace rt {

Ray::Ray(const Point& o, const Vector& d): o(o), d(d)
{
    rt_assert(d != Vector::rep(0.0f));
    this->inv_d = Vector( 1 / d.x, 1 / d.y, 1 / d.z);
}

Point Ray::getPoint(float distance) const {
    return o + (d * distance);
}

}
