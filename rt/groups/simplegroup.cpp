#include <rt/groups/simplegroup.h>
#include <core/assert.h>

namespace rt {

BBox SimpleGroup::getBounds() const {
    /* TODO */ NOT_IMPLEMENTED;
}

Intersection SimpleGroup::intersect(const Ray& ray, float tmin, float tmax) const {
    /* TODO */
    Intersection intersection = Intersection::failure();
    Intersection result;
    for (auto &object : primitives)
    {
        result = object->intersect(ray, tmin, tmax);
        if (result)
        {
            rt_assert(result.distance <= tmax && result.distance >= tmin);
            intersection = result;
            tmax = intersection.distance;
        }
    }
    return intersection;
}

void SimpleGroup::rebuildIndex() {
    //do nothing
}

void SimpleGroup::add(Primitive* p) {
    /* TODO */
    this->primitives.push_back(p);
}

void SimpleGroup::setMaterial(Material* m) {
    /* TODO */ NOT_IMPLEMENTED;
}

void SimpleGroup::setCoordMapper(CoordMapper* cm) {
    /* TODO */ NOT_IMPLEMENTED;
}

}
