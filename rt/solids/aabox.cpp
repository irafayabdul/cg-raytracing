#include <rt/solids/aabox.h>
#include <core/assert.h>

namespace rt {

AABox::AABox(const Point& corner1, const Point& corner2, CoordMapper* texMapper, Material* material)
: Solid(texMapper, material)
{
    rt_assert(corner1 != corner2);
    this->corner1 = corner1;
    this->corner2 = corner2;

}

BBox AABox::getBounds() const {
    /* TODO */ NOT_IMPLEMENTED;
}

Solid::Sample AABox::sample() const {
    NOT_IMPLEMENTED;
}

float AABox::getArea() const {
    Vector vec = max(corner1, corner2) - min(corner1, corner2);
    return  (2 * vec.x * vec.y) + (2 * vec.x * vec.z) + (2 * vec.y * vec.z);
}

Intersection AABox::intersect(const Ray& ray, float tmin, float tmax) const {
    float nx = (corner1.x - ray.o.x) * ray.inv_d.x;
    float fx = (corner2.x - ray.o.x) * ray.inv_d.x;
    float ny = (corner1.y - ray.o.y) * ray.inv_d.y;
    float fy = (corner2.y - ray.o.y) * ray.inv_d.y;
    float nz = (corner1.z - ray.o.z) * ray.inv_d.z;
    float fz = (corner2.z - ray.o.z) * ray.inv_d.z;
    float _tmin = max(max(min(nx, fx), min(ny, fy)), min(nz, fz));
    float _tmax = min(min(max(nx, fx), max(ny, fy)), max(nz, fz));
    if (_tmax < _tmin || _tmin > tmax || _tmin < tmin)
    {
        return Intersection::failure();
    }
    //get the hitpoint
    Point hitPoint = ray.getPoint(_tmin);

    // get the face of hitpoint
    Vector hitCorner1 = hitPoint - corner1;
    Vector hitCorner2 = hitPoint - corner2;

    Vector fNormal;
    float fDirection;
    if (fabsf(hitCorner1.x) < epsilon || fabsf(hitCorner2.x) < epsilon)
    {
        fDirection = fabsf(hitCorner2.x) < epsilon ? hitCorner1.x - hitCorner2.x : hitCorner2.x - hitCorner1.x;
        fNormal = Vector((fDirection > 0) - (fDirection < 0), 0, 0);
    }
    else if (fabsf(hitCorner1.y) < epsilon || fabsf(hitCorner2.y) < epsilon)
    {
        fDirection = fabsf(hitCorner2.y) < epsilon ? hitCorner1.y - hitCorner2.y : hitCorner2.y - hitCorner1.y;
        fNormal = Vector(0, (fDirection > 0) - (fDirection < 0), 0);
    }
    else if (fabsf(hitCorner1.z) < epsilon || fabsf(hitCorner2.z) < epsilon)
    {
        fDirection = fabsf(hitCorner2.z) < epsilon ? hitCorner1.z - hitCorner2.z : hitCorner2.z - hitCorner1.z;
        fNormal = Vector(0, 0, (fDirection > 0) - (fDirection < 0));
    }
    else
    {
        return Intersection::failure();
    }

    return Intersection(_tmin, ray, this, fNormal, hitPoint);
}

}
