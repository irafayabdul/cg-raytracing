#include <rt/solids/quad.h>
#include <core/assert.h>

namespace rt {

Quad::Quad(const Point& origin, const Vector& span1, const Vector& span2, CoordMapper* texMapper, Material* material)
: Solid(texMapper, material), base(span1), edge(span2)
{
    rt_assert(base != edge);

    normal = cross(
                        base,
                        edge)
                        .normalize();

    vertices[0] = origin;
    vertices[1] = origin + base;
    vertices[2] = origin + base + edge;
    vertices[3] = origin + edge;
}

BBox Quad::getBounds() const {
    /* TODO */ NOT_IMPLEMENTED;
}

Intersection Quad::intersect(const Ray& ray, float tmin, float tmax) const {
    /* TODO */
    float u, v, t;
    Vector tvec;
    Vector pvec;
    Vector qvec = rt::cross(ray.d, base);
    float det = rt::dot(edge, qvec);
    float inv_det = 1 / det;

    tvec = ray.o - vertices[0];
    u = rt::dot(tvec, qvec) * inv_det;

    if (u < 0.0f || u > 1.0f)
        return Intersection::failure();

    pvec = rt::cross(tvec, edge);
    v = rt::dot(ray.d, pvec) * inv_det;
    if (v < 0.0f || v > 1.0f)
        return Intersection::failure();

    t = rt::dot(base, pvec) * inv_det;
    if (t > tmin && t < tmax)
        return Intersection(t, ray, this, normal, Point(v, u, 0.f));
    else
        return Intersection::failure();
}

Solid::Sample Quad::sample() const {
    /* TODO */ NOT_IMPLEMENTED;
}

float Quad::getArea() const { 
    Vector perpendicular = edge - rt::dot(edge, base) * base;
    return perpendicular.length() * base.length();
}

}
