#include <rt/solids/disc.h>

namespace rt {

Disc::Disc(const Point& center, const Vector& normal, float radius, CoordMapper* texMapper, Material* material)
: Solid(texMapper, material), center(center), normal(normal.normalize()), radius(radius)
{
}

BBox Disc::getBounds() const {
    /* TODO */ NOT_IMPLEMENTED;
}

Intersection Disc::intersect(const Ray& ray, float tmin, float tmax) const {
    float proj = dot(ray.d, normal);
    if (fabsf(proj) < epsilon)
    {
        return Intersection::failure();
    }
    float dist = dot((center - ray.o), normal);
    float t = dist / proj;
    Point local = ray.getPoint(t);
    if (t < tmax && t > tmin && (local - center).length() < radius)
        return Intersection(t, ray, this, normal, local);
    else
        return Intersection::failure();
}

Solid::Sample Disc::sample() const {
    NOT_IMPLEMENTED;
}

float Disc::getArea() const {
     return pi * pow(this->radius,2);
}

}
