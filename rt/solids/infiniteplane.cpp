#include <rt/solids/infiniteplane.h>

namespace rt {

InfinitePlane::InfinitePlane(const Point& origin, const Vector& normal, CoordMapper* texMapper, Material* material)
: Solid(texMapper, material), center(origin), normal(normal.normalize())
{

}

BBox InfinitePlane::getBounds() const {
    /* TODO */ NOT_IMPLEMENTED;
}

Intersection InfinitePlane::intersect(const Ray& ray, float tmin, float tmax) const {
    /* TODO */
    float proj = dot(ray.d, normal);
    if (fabsf(proj) < epsilon)
    {
        return Intersection::failure();
    }
    float dist = dot((center - ray.o), normal);
    float t = dist / proj;
    Point local = ray.getPoint(t);
    if (t < tmax && t > tmin)
        return Intersection(t, ray, this, normal, local);
    else
        return Intersection::failure();
}

Solid::Sample InfinitePlane::sample() const {
    /* TODO */ NOT_IMPLEMENTED;
}

float InfinitePlane::getArea() const {
    /* TODO */
    return FLT_MAX;
}

}
