#include <rt/solids/sphere.h>

namespace rt {

Sphere::Sphere(const Point& center, float radius, CoordMapper* texMapper, Material* material)
: Solid(texMapper, material), center(center), radius(radius)
{

}

BBox Sphere::getBounds() const {
    /* TODO */ NOT_IMPLEMENTED;
}

Intersection Sphere::intersect(const Ray& ray, float tmin, float tmax) const { 
    //using algebric approach to solve t
    float a = rt::dot(ray.d, ray.d);
    float b = 2 * rt::dot(ray.d, ray.o - center);
    float c = rt::dot(ray.o - center, ray.o - center) - pow(radius,2);

    float discriminant = pow(b,2) - 4 * a * c;
    if (discriminant < 0)
        return Intersection::failure();

    float t1 = (-b + sqrt(discriminant)) / (2 * a);
    float t2 = (-b - sqrt(discriminant)) / (2 * a);
    if (t1 < 0)
        t1 = FLT_MAX;
    if (t2 < 0)
        t2 = FLT_MAX;

    float t = min(t1, t2);
    if (t < tmax && t > tmin)
    {
        Point hitPoint = ray.getPoint(t);
        Vector normal = (hitPoint - center).normalize();
        return Intersection(t, ray, this, normal, hitPoint);
    }
    else
        return Intersection::failure();
}

Solid::Sample Sphere::sample() const {
	NOT_IMPLEMENTED;
}

float Sphere::getArea() const {
    /* TODO */
    return 4 * pi * this->radius * this->radius;
}

}
