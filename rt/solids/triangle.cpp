#include <rt/solids/triangle.h>

namespace rt {

Triangle::Triangle(Point vertices[3], CoordMapper* texMapper, Material* material)
: Solid(texMapper, material)
{
    /* TODO */
    this->vertices[0] = vertices[0];
    this->vertices[1] = vertices[1];
    this->vertices[2] = vertices[2];
    base = vertices[1] - vertices[0];
    edge = vertices[2] - vertices[0];
    normal = (rt::cross(base, edge)).normalize();
    
}

Triangle::Triangle(const Point& v1, const Point& v2, const Point& v3, CoordMapper* texMapper, Material* material)
: Solid(texMapper, material)
{
    vertices[0] = v1;
    vertices[1] = v2;
    vertices[2] = v3;
    base = vertices[1] - vertices[0];
    edge = vertices[2] - vertices[0];
    normal = (rt::cross(base, edge)).normalize();
}

BBox Triangle::getBounds() const {
    /* TODO */ NOT_IMPLEMENTED;
}

Intersection Triangle::intersect(const Ray& ray, float tmin, float tmax) const {
    // Moller Trumbore
    float t, u, v;
    Vector tvec;
    Vector pvec;
    Vector qvec = rt::cross(ray.d, edge);
    float det = rt::dot(base, qvec);
    float inv_det;

    inv_det = 1 / det;
    tvec = ray.o - vertices[0];
    u = rt::dot(tvec, qvec) * inv_det;

    if (u < 0.0f || u > 1.0f)
        return Intersection::failure();

    pvec = rt::cross(tvec, base);
    v = rt::dot(ray.d, pvec) * inv_det;
    if (v < 0.0f || u + v > 1.0f)
        return Intersection::failure();

    t = rt::dot(edge, pvec) * inv_det;
    if (t > tmin && t < tmax)
        return Intersection(t, ray, this, normal, Point(1 - u - v, u, v));
    else
        return Intersection::failure();
}

Solid::Sample Triangle::sample() const {
    /* TODO */ NOT_IMPLEMENTED;
}

float Triangle::getArea() const {
    Vector perpendicular = edge - rt::dot(edge, base) * base;
    return perpendicular.length() * base.length() / 2.f;
}

}