#include <rt/cameras/perspective.h>
#include <math.h>

namespace rt {

PerspectiveCamera::PerspectiveCamera(const Point& center, const Vector& forward, const Vector& up, float verticalOpeningAngle, float horizontalOpeningAngle)
:   Camera(center, forward, up),
    verticalOpeningAngle(verticalOpeningAngle),
    horizontalOpeningAngle(horizontalOpeningAngle)
{}

Ray PerspectiveCamera::getPrimaryRay(float x, float y) const {
    Vector dx = x * tan(horizontalOpeningAngle / 2) * right;
    Vector dy = y * tan(verticalOpeningAngle / 2) * up;
    return Ray(
        center,
        (forward + dx + dy).normalize()
    );
}
 
}
