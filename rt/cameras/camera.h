#ifndef CG1RAYTRACER_CAMERAS_CAMERA_HEADER
#define CG1RAYTRACER_CAMERAS_CAMERA_HEADER

#include <core/assert.h>
#include <rt/ray.h>
#include <core/vector.h>

namespace rt {

class Camera {
public:
    Camera(
        const Point& center,
        const Vector& forward,
        const Vector& up
        ) : center(center), 
            forward(forward.normalize()), 
            up(up.normalize()), 
            right(cross(this->forward, this->up).normalize())
        {
            this->up = rt::cross(right, this->forward).normalize();
        }

    virtual Ray getPrimaryRay(float x, float y) const = 0;

protected:
    Point center;
    Vector forward;
    Vector up;
    Vector right;
};

}

#endif
