#include <rt/cameras/fisheye.h>
#include <core/point.h>
#include <core/vector.h>
#include <math.h>
#include <core/scalar.h>
#include <rt/ray.h>

namespace rt {

FisheyeCamera::FisheyeCamera(const Point& center, const Vector& forward, const Vector& up, float openingAngle)
:   Camera(center, forward, up),
    openingAngle(openingAngle) 
{
}

Ray FisheyeCamera::getPrimaryRay(float x, float y) const {
    //Polar coordinates of ssc
    float r = sqrtf(sqr(x) + sqr(y));
    float phi = atan2f(y, x);
    
    float theta = r * openingAngle / 2;

    return Ray(
        center,
        sinf(phi) * cosf(theta) * right +
        sinf(theta) * up +
        cosf(phi) * cosf(theta) * forward
        );
}

}
