#include <rt/cameras/orthographic.h>
#include <rt/ray.h>
#include <core/vector.h>

namespace rt {

OrthographicCamera::OrthographicCamera(const Point& center, const Vector& forward, const Vector& up, float scaleX, float scaleY)
:   Camera(center, forward, up),
    scaleX(scaleX), 
    scaleY(scaleY) 
{

}

Ray OrthographicCamera::getPrimaryRay(float x, float y) const {
    /* TODO */ 
    return Ray(
        center + ((y * scaleY * up)/ 2.) + ((x * scaleX * right)/ 2.),
        forward
        );
}

}
