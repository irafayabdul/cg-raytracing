#include <rt/cameras/environment.h>
#include <core/point.h>
#include <core/vector.h>
#include <math.h>
#include <core/scalar.h>
#include <rt/ray.h>


namespace rt {

EnvironmentCamera::EnvironmentCamera(const Point& center, const Vector& forward, const Vector& up, float verticalOpeningAngle, float horizontalOpeningAngle)
:   Camera(center, forward, up),
    verticalOpeningAngle(verticalOpeningAngle), 
    horizontalOpeningAngle(horizontalOpeningAngle) 
{
}

Ray EnvironmentCamera::getPrimaryRay(float x, float y) const {
    float phi = x * horizontalOpeningAngle / 2.f;
    float theta = y * verticalOpeningAngle / 2.f;
    return Ray(
        center, 
        sinf(phi) * cosf(theta) * right +
        sinf(theta) * up + 
        cosf(phi) * cosf(theta) * forward);
}

}
