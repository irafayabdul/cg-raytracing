#include <core/scalar.h>
#include <core/image.h>
#include <rt/renderer.h>
#include <rt/ray.h>
#include <iostream>
#include <rt/cameras/camera.h>
#include <rt/integrators/integrator.h>

namespace rt {

Renderer::Renderer(Camera* cam, Integrator* integrator)
    : cam(cam), integrator(integrator), samples(1)
{}

void Renderer::render(Image& img) {
    /* TODO */
    int width = img.width();
     int height = img.height();
     for (int i = 0; i < width; i++) {
         std::cout << i << " ";
         float uniform_x = ((i + 0.5) * 2. / width) - 1; //pixel raster coords to screen space coord, in [-1, 1]
         for (int j = 0; j < height; j++) {
             float uniform_y = ((j + 0.5) * 2. / height) - 1; //pixel raster coords to screen space coord, in [-1, 1]
             img(i, j) = integrator->getRadiance(cam->getPrimaryRay(uniform_x, -uniform_y)); //y axis flip
         }
     }
}

}

rt::RGBColor a1computeColor(rt::uint x, rt::uint y, rt::uint width, rt::uint height);

namespace rt {

void Renderer::test_render1(Image& img) {
    CG_UNUSED(img);
    int width = img.width();
    int height = img.height();
    for (int i = 0; i < width; i++)
    {
        // std::cout << i << " ";
        for (int j = 0; j < height; j++)
        {
            img(i, j) = a1computeColor(i, j, width, height);
        }
    }
}
}

rt::RGBColor a2computeColor(const rt::Ray& r);

namespace rt {

void Renderer::test_render2(Image& img) {
    CG_UNUSED(img);
    int width = img.width();
    int height = img.height();
    for (int i = 0; i < width; i++)
    {
        // std::cout << i << " ";
        float uniform_x = ((i + 0.5) * 2. / width) - 1; //pixel raster coords to screen space coord, in [-1, 1]
        for (int j = 0; j < height; j++)
        {
            float uniform_y = ((j + 0.5) * 2. / height) - 1; //pixel raster coords to screen space coord, in [-1, 1]
            img(i, j) = a2computeColor(cam->getPrimaryRay(uniform_x, -uniform_y)); //y axis flip
        }
    }
}

}
