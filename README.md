# CG-RayTracing

Abdul Rafay (7024258) \
Filippo Garosi

# Assignment 1
## Implementation
- Vector
- Point
- RGB
- Ray
- PerspectiveCamera
- OrthographicCamera
- FishEyeCamera
- EnvironmentCamera

References:\
https://pbr-book.org/3ed-2018/contents \
https://www.scratchapixel.com/lessons/3d-basic-rendering/perspective-and-orthographic-projection-matrix/building-basic-perspective-projection-matrix \
https://www.youtube.com/watch?v=oYOpEkaeq_o \
https://docs.opencv.org/3.4/db/d58/group__calib3d__fisheye.html \
https://www.mathematik.uni-marburg.de/~thormae/lectures/graphics1/graphics_6_1_eng_web.html#1 \
