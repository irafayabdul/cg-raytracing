#include <core/color.h>
#include <core/scalar.h>
#include <core/homogeneouscoord.h>

namespace rt {

RGBColor::RGBColor(const HomogeneousCoord& coord)
{
/* TODO */ NOT_IMPLEMENTED;
}

RGBColor RGBColor::operator + (const RGBColor& c) const {
    return RGBColor(r + c.r, g + c.g, b + c.b);
}

RGBColor RGBColor::operator - (const RGBColor& c) const {
    return RGBColor( r - c.r, g - c.g, b - c.b );
}

RGBColor RGBColor::operator * (const RGBColor& c) const {
    return RGBColor( r * c.r, g * c.g, b * c.b );
}

bool RGBColor::operator == (const RGBColor& c) const {
    return fabsf(r - c.r) < epsilon && fabsf(g - c.g) < epsilon && fabsf(b - c.b) < epsilon;
}

bool RGBColor::operator != (const RGBColor& c) const {
    return fabsf(r - c.r) > epsilon || fabsf(g - c.g) > epsilon || fabsf(b - c.b) > epsilon;
}

RGBColor RGBColor::clamp() const {
    return RGBColor(
        max(min(r, float(1.0)), float(0.)),
        max(min(g, float(1.0)), float(0.)),
        max(min(b, float(1.0)), float(0.))
    );
}

RGBColor RGBColor::gamma(float gam) const {
    /* TODO */ NOT_IMPLEMENTED;
}

float RGBColor::luminance() const {
    /* TODO */ NOT_IMPLEMENTED;
}

RGBColor operator * (float scalar, const RGBColor& c) {
    return RGBColor(c.r * scalar, c.g * scalar, c.b * scalar
    );
}

RGBColor operator * (const RGBColor& c, float scalar) {
    return RGBColor( c.r * scalar, c.g * scalar, c.b * scalar
    );
}

RGBColor operator / (const RGBColor& c, float scalar) {
    return RGBColor( c.r / scalar, c.g / scalar, c.b / scalar
    );
}

}
