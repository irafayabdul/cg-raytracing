#include <core/scalar.h>
#include <core/vector.h>
#include <core/point.h>
#include <core/homogeneouscoord.h>
#include <core/assert.h>
#include <algorithm>
#include <cmath>

namespace rt {

Vector::Vector(float x, float y, float z) : x(x), y(y), z(z)
{
}

Vector::Vector(const HomogeneousCoord& coord)
{
    /* TODO */
}

Vector Vector::operator + (const Vector& b) const {
    return Vector( x + b.x, y + b.y, z + b.z);
}

Vector Vector::operator - (const Vector& b) const {
    return Vector( x - b.x, y - b.y, z - b.z );
}

Vector Vector::operator - () const {
    return Vector( - x, - y, - z );
}

Vector Vector::normalize() const {
    return (*this) / length();
}

Vector operator * (float scalar, const Vector& b) {
    return Vector( b.x * scalar, b.y * scalar, b.z * scalar
    );
}

Vector operator * (const Vector& a, float scalar) {
    return Vector( a.x * scalar, a.y * scalar, a.z * scalar
    );
}

Vector operator / (const Vector& a, float scalar) {
    return Vector( a.x / scalar, a.y / scalar, a.z / scalar
    );
}

Vector cross(const Vector& a, const Vector& b) {
    return Vector(
        a.y * b.z - a.z * b.y,
        a.z * b.x - a.x * b.z,
        a.x * b.y - a.y * b.x
    );
}

float dot(const Vector& a, const Vector& b) {
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

float Vector::lensqr() const {
    return sqr(x) + sqr(y) + sqr(z); 
}

float Vector::length() const {
    return sqrtf(lensqr());
}


bool Vector::operator == (const Vector& b) const {
    return fabsf(x - b.x) < epsilon && fabsf(y - b.y) < epsilon && fabsf(z - b.z) < epsilon;
}

bool Vector::operator != (const Vector& b) const {
    return fabsf(x - b.x) > epsilon || fabsf(y - b.y) > epsilon || fabsf(z - b.z) > epsilon;
}

Vector min(const Vector& a, const Vector& b) {
    return Vector( min(a.x, b.x), min(a.y, b.y), min(a.z, b.z)
    );
}

Vector max(const Vector& a, const Vector& b) {
    return Vector( max(a.x, b.x), max(a.y, b.y), max(a.z, b.z)
    );
}

Point operator + (const Point& a, const Vector& b) {
    return Point( a.x + b.x, a.y + b.y, a.z + b.z
    );
}

Point operator + (const Vector& a, const Point& b) {
    return Point( a.x + b.x, a.y + b.y, a.z + b.z
    );
}

Point operator - (const Point& a, const Vector& b) {
    return Point( a.x - b.x, a.y - b.y, a.z - b.z
    );
}

Point operator * (const HomogeneousCoord& scale, const Point& p) {
    /* TODO */ NOT_IMPLEMENTED;
}

}
