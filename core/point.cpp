#include <core/point.h>
#include <core/homogeneouscoord.h>
#include <core/scalar.h>
#include <core/assert.h>
#include <core/vector.h>

namespace rt {

Point::Point(float x, float y, float z) : x(x), y(y), z(z)
{

}

Point::Point(const HomogeneousCoord& coord)
{
    /* TODO */ NOT_IMPLEMENTED;
}

Vector Point::operator - (const Point& b) const {
    return Vector( x - b.x, y - b.y, z - b.z);
}

bool Point::operator == (const Point& b) const {
    return fabsf(x - b.x) < epsilon && fabsf(y - b.y) < epsilon && fabsf(z - b.z) < epsilon;
}

bool Point::operator != (const Point& b) const {
    return fabsf(x - b.x) > epsilon || fabsf(y - b.y) > epsilon || fabsf(z - b.z) > epsilon;
}

Point operator * (float scalar, const Point& b) {
    return Point(b.x * scalar, b.y * scalar, b.z * scalar);
}

Point operator * (const Point& a, float scalar) {
    return Point( a.x * scalar, a.y * scalar, a.z * scalar);
}

Point min(const Point& a, const Point& b) {
    return Point( min(a.x, b.x), min(a.y, b.y), min(a.z, b.z));
}

Point max(const Point& a, const Point& b) {
    return Point( max(a.x, b.x), max(a.y, b.y), max(a.z, b.z));
}

}
